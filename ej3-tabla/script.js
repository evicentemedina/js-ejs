// Coger y guardar el botón en la variable 'btnGenerar':
var btnGenerar = document.getElementsByTagName('button')[0];

// Coger y guardar ambos 'input':
var inputs = document.getElementsByTagName('input');
var inputFilas = inputs[0];
var inputColumnas = inputs[1];

/* Establecer las propiedades de los 'input':
 * Atención a que en el html sólo las tiene puestas el primero,
 * pero con JS se les ponen a ambos. Aunque se borren en el html, seguirían
 * teniéndolas por el JS.
 */
for (let i = 0; i < inputs.length; i++) {
  inputs[i].type = 'number';
  inputs[i].min = 0;
  inputs[i].value = 10;
}

// Coger el cuerpo de la tabla, 'tbody', y guardarlo:
var tabla //= *Rellenar*;

// Declarar la función de generar tabla:
function generarTabla() {
  // *Generar tabla*
}

// Añadir la función al evento de click del botón:
btnGenerar.onclick = function() {
  generarTabla();
};
