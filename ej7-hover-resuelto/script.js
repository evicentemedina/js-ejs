var colorRectangles = document.getElementsByTagName('rect');
var circle = document.getElementById('circle');
var square = document.getElementById('square');
var triangle = document.getElementById('triangle');
var figures = [circle, square, triangle];
var select = document.getElementsByTagName('select')[0];
var color = '';
var areaP = document.getElementById('area');

function changeFigureColor(color) {
  let i = select.value;
  figures[i].style.fill = color;
}

for (let i = 0; i < colorRectangles.length; i++) {
  colorRectangles[i].onmouseover = function() {
    changeFigureColor(colorRectangles[i].id);
  }
  colorRectangles[i].onmouseout = function() {
    changeFigureColor(color);
  }
  colorRectangles[i].onclick = function() {
    color = colorRectangles[i].id;
    changeFigureColor(color);
  }
}

circle.onclick = function() {
  console.log(circle)
  areaP.innerText = 'Área: ' + (
    2 * Math.PI * Math.pow(circle.attributes.r.value, 2)
  ) + 'px';
}

square.onclick = function() {
  console.log(square)
  areaP.innerText = 'Área: ' + (
    square.attributes.width.value * square.attributes.height.value
  ) + 'px';
}

triangle.onclick = function() {
  console.log(triangle)
  areaP.innerText = 'Área: ' + (400 * 400 / 2) + 'px';
}

function changeFigure() {
  areaP.innetText = '';
  for (let i = 0; i < figures.length; i++) {
    if (i == select.value) {
      figures[i].style.visibility = 'visible';
    } else {
      figures[i].style.visibility = 'hidden';
    }
  }
}

changeFigure();
