var maxInputLength = 120;
var notes = [];

var inputNote = document.getElementsByTagName('input')[0];
var spanCharCounter = document.getElementsByTagName('span')[0];
var sectionNotes = document.getElementsByTagName('section')[0];

inputNote.maxLength = maxInputLength;

// Cuando el input está activo
inputNote.onfocus = function() {
}

// Cuando el input pierde el foco
inputNote.onblur = function() {
}

// Cuando el valor del input cambia
inputNote.oninput = function() {
}

// Cuando se pulsa cualquier tecla
inputNote.onkeypress = function(event) {
}

// Damos foco por defecto al input
inputNote.focus();
