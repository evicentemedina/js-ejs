var imgs = document.getElementsByTagName('img');
var btn = document.getElementsByTagName('button')[0];
var encendida = false;

function encenderApagar() {
  if (!encendida) {
    imgs[0].style = 'visibility: hidden';
    imgs[1].style = 'visibility: visible';
    //btn.innerText = 'Apagar';
    imgs[2].style = 'visibility: hidden';
    imgs[3].style = 'visibility: visible';
  } else {
    imgs[0].style = 'visibility: visible';
    imgs[1].style = 'visibility: hidden';
    //btn.innerText = 'Encender';
    imgs[2].style = 'visibility: visible';
    imgs[3].style = 'visibility: hidden';
  }
  encendida = !encendida;
}
