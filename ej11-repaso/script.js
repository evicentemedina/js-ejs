var vocales = 'aeiou'.split('');
var letrasDni = 'TRWAGMYFPDXBNJZSQVHLCKET'.split('');

var titulo = document.getElementsByTagName('h1')[0];

function cambiarColorTitulo(color) {
  titulo.style.color = color;
}

function cambiarFondoTitulo(color) {
  titulo.style.backgroundColor = color;
}

function contarVocales() {
  var inputVocales = document.getElementById('inputVocales');
  var inputValueArray = inputVocales.value.split('');
  var cont = 0;
  for (let i = 0; i < inputValueArray.length; i++) {
    for (let j = 0; j < vocales.length; j++) {
      if (inputValueArray[i] == vocales[j]) {
        cont++;
      }
    }
  }
  var resultado = document.getElementById('resultadoVocales');
  resultado.innerText = cont;
}

function cambiarMayus() {
  var inputMayus = document.getElementById('inputMayus');
  var resultado = document.getElementById('resultadoMayus');
  resultado.innerText = inputMayus.value.toUpperCase();
}

function calcularLetraDni() {
  var inputDniValue = document.getElementById('inputDni').value;
  var spanResultado = document.getElementById('resultadoDni')
  if (inputDniValue == '') {
    spanResultado.innerText = 'Introduzca ocho números.';
  } else if (isNaN(inputDniValue)) {
    spanResultado.innerText = 'Introduzca sólo caracteres numéricos.';
  } else if (inputDniValue.length < 8) {
    spanResultado.innerText = 'No introduzca menos de ocho números.';
  } else if (inputDniValue.length > 8) {
    spanResultado.innerText = 'No introduzca más de ocho números.';
  } else {
    inputDniValue = Number(inputDniValue);
    var letra = letrasDni[inputDniValue % 23];
    spanResultado.innerText = letra;
  }
}

function generarRectangulo() {
}

function cambiarColorRect(color) {
}
