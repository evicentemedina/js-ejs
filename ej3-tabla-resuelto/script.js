var inputs = document.getElementsByTagName('input');
var button = document.getElementsByTagName('button')[0];
var tableb = document.getElementsByTagName('tbody')[0];

for (let input of inputs) {
  input.type = 'number';
  input.min = 0;
  input.value = 10;
}

button.onclick = function() {
  let rows = inputs[0].value;
  let columns = inputs[1].value;
  let tbody = '';
  for (let r = 0; r < rows; r++) {
    tbody += '<tr>';
    for (let c = 0; c < columns; c++) {
      if (r === 0 && c === 0) {
        tbody += '<td>0</td>';
      } else if (r === 0) {
        tbody += '<td>' + c + '</td>';
      } else if (c === 0) {
        tbody += '<td>' + r + '</td>';
      } else {
        tbody += '<td></td>';
      }
    }
    tbody += '</tr>';
  }
  tableb.innerHTML = tbody;
}
