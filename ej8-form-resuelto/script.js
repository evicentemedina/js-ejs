// Recogemos todos los elementos necesarios:
var inputs = document.getElementsByTagName('input');
var inputEmail = inputs[1];
var inputPassword1 = inputs[2];
var inputPassword2 = inputs[3];
var buttons = document.getElementsByTagName('button');
var btnReset = buttons[0];
var btnSubmit = buttons[1];
var errorMsg = document.getElementById('errorMsg');
var successMsg = document.getElementById('successMsg');

// Función del botón de Reiniciar:
btnReset.onclick = function() {
  // Recorremos todos los inputs para vaciar su valor y color de fondo:
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].value = '';
    inputs[i].style.backgroundColor = '';
  }
  // Vaciamos el span del mensaje de error:
  errorMsg.innerText = '';
  // Ocultamos el span del mensaje de éxito:
  successMsg.style.opacity = 0;
}

// Función de validación de que ningún input esté vacío:
function validateNonEmpty() {
  // Asumimos en principio que ninguno está vacío:
  let nonEmpty = true;
  // Recorremos el array de inputs:
  for (let i = 0; i < inputs.length; i++) {
    // Comprobamos por cada uno si está vacío:
    if (inputs[i].value == '') {
      // Si lo está, ponemos el color de fondo a rojo:
      inputs[i].style.backgroundColor = 'red';
      // Asignamos el mensaje de error al texto del span:
      errorMsg.innerText = 'Ningún campo puede estar vacío.';
      // Y cambiamos la variable de 'no vacío' a falso:
      nonEmpty = false;
    } else {
      // Si sí tiene algún valor, reiniciamos su color de fondo:
      inputs[i].style.backgroundColor = '';
    }
  }
  // Por último, devolvemos la variable de 'no vacío', que estará en 'false'
  // si algún input estaba vacío y en 'true' si todos tenían algún valor:
  return nonEmpty;
}

// Función de validación del email:
function validateEmail() {
  // Lo primero es recoger el valor actual que tenga el input:
  let email = inputEmail.value;
  // Hacemos un split de '@' y lo guardamos en una variable por comodidad:
  let emailArr = email.split('@');
  // Si la longitud del array es distinto de 2, es que o había más de una '@',
  // o no había ninguna:
  if (emailArr.length != 2) {
    // Por lo que terminamos la función devolviendo un 'false':
    return false;
  // Si había exactamente una '@', comprobamos si la parte a la izquierda está
  // vacía (ej.: @gmail.com):
  } else if (emailArr[0] == '') {
    return false;
  // Si no lo estaba, pasamos a hacer un split de la parte a la derecha de la
  // '@' para comprobar que contenga exactamente un '.':
  } else if (emailArr[1].split('.').length != 2) {
    return false;
  // Comprobamos que la parte entre la '@' y el '.' no esté vacía
  // (ej.: usuario@.com):
  } else if (emailArr[1].split('.')[0] == '') {
    return false;
  // Y comprobamos que la parte a la derecha del '.' tampoco esté vacía
  // (ej.: usuario@gmail.):
  } else if (emailArr[1].split('.')[1] == '') {
    return false;
  // Si se han pasado todas las comprobaciones, asumimos que el email
  // es correcto:
  } else {
    return true;
  }
}

// Función de validación de los campos de contraseña:
function validatePassword() {
  // Comprobamos que el valor de ambos input no sea diferente:
  if (inputPassword1.value != inputPassword2.value) {
    // Si lo es, ponemos sus colores de fondo a rojo:
    inputPassword1.style.backgroundColor = 'red';
    inputPassword2.style.backgroundColor = 'red';
    // Y el mensaje de error:
    errorMsg.innerText = 'Las contraseñas no coinciden.';
    // Devolvemos 'false' para poder saber si ha fallado:
    return false;
  } else {
    // Y 'true' para saber si sí coinciden:
    return true;
  }
}

// Función del botón de enviar:
btnSubmit.onclick = function() {
  // Ocultamos el mensaje de éxito, por si estaba visible debido a un clic
  // anterior en el botón de enviar que sí hubiera tenido éxito:
  successMsg.style.opacity = 0;
  // Llamamos primero a la función que comprueba que ningún input esté
  // vacío, evaluando su resultado con un 'if' para decidir si seguimos
  // con las comprobaciones o no:
  if (validateNonEmpty()) {
    // Si todos los input tenían valor, pasamos a llamar a la función de
    // comprobación del email, en este caso entrando en el 'if' si el email
    // es incorrecto:
    if (!validateEmail()) {
      // Así podemos poner su color de fondo a rojo y el mensaje de error sin
      // tener que repetir código:
      inputEmail.style.backgroundColor = 'red';
      errorMsg.innerText = 'El email debe cumplir el formato: usuario@servicio.correo';
    // Si la función de validación del email ha tenido éxito, pasamos a llamar
    // a la función de validación de contraseña:
    } else if (validatePassword()) {
      // Si devuelve 'true', pasamos a eliminar el color de fondo de todos los
      // input mediante un bucle 'for':
      for (let i = 0; i < inputs.length; i++) {
        inputs[i].style.backgroundColor = '';
      }
      // También vaciamos el texto del mensaje de error:
      errorMsg.innerText = '';
      // Y mostramos el mensaje de éxito poniendo su opacidad a '1':
      successMsg.style.opacity = 1;
    }
  }
}
