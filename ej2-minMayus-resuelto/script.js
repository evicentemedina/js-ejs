/*
 * Ejercicio de JavaScript: transformar un texto a minúsculas y a mayúsculas.
 */

// Se cogen todos los elementos con la clase 'elementos':
var arrayElementos = document.getElementsByClassName('elementos');

// Se almacena cada uno en una variable:
var textArea = arrayElementos[0];
var divResultado = arrayElementos[1];

function cambiarMayus() {

    // Se coge el contenido actual del textarea:
    let texto = textArea.value;

    // Se sobreescribe la variable 'texto' con todos los caracteres
    // en mayúsculas:
    texto = texto.toUpperCase();

    // Finalmente, se asigna el texto en mayúsculas al texto del 'div' para
    // mostrar el resultado:
    divResultado.innerText = texto;
}

function cambiarMinus() {

    // Se coge el contenido actual del textarea:
    let texto = textArea.value;

    // Se sobreescribe la variable con todos los caracteres en minúsculas:
    texto = texto.toLowerCase();

    // Y se asigna el texto transformado al texto del 'div' del resultado:
    divResultado.innerText = texto;
}

function cambiarPalabrasMayus() {

    // Se coge el contenido actual del textarea:
    // (Atención a que se puede usar el primer elemento del array de elementos,
    // ya que tiene el mismo contenido que la variable 'textArea')
    let texto = arrayElementos[0].value;

    // Se crea un array para las palabras y se le asigna el texto dividido
    // por los espacios:
    let arrayPalabras = texto.split(' ');

    // Variable para el resultado, de momento una string vacía:
    let resultado = '';

    // Variable auxiliar para distinguir la primera vez que se ejecute el
    // bucle 'for':
    // (Ésto es necesario si no queremos añadir un espacio de más al principio)
    let primeraVez = true;

    // Se recorre el array de palabras para transformar la primera letra de
    // cada una a mayúscula:
    //for (let palabra of arrayPalabras) {
    // Nota: se podría usar un bucle 'for' normal:
    for (let i = 0; i < arrayPalabras.length; i++) {
    // Simplemente cambiaría 'palabra' por 'arrayPalabras[i]'.

        // Si es la primera iteración, no se añade espacio:
        if (primeraVez) {
            primeraVez = false;
        } else {
            // Si no, se añade uno al resultado para separar las palabras:
            resultado += ' ';
        }

        // Se divide cada palabra en un array de letras:
        //let letras = palabra.split('');
        let letras = arrayPalabras[i].split('');

        // Se transforma sólo la primera a mayúscula y se añade al resultado:
        resultado += letras[0].toUpperCase();

        // Ahora se recorre el array de letras a partir de la segunda posición
        // para añadir el resto de letras al resultado:
        for (let l = 1; l < letras.length; l++) {
            resultado += letras[l];
        }
    }

    // Por último, se asigna el resultado al texto del 'div' de resultado:
    divResultado.innerText = resultado;
}
