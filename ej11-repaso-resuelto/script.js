var titulo = document.getElementsByTagName('h1')[0];
var vocales = 'aeiou'.split('');
var letrasDni = 'TRWAGMYFPDXBNJZSQVHLCKET'.split('');

function cambiarColorTitulo(color) {
  titulo.style.color = color;
}

function cambiarFondoTitulo(color) {
  titulo.style.backgroundColor = color;
}

function contarVocales() {
  var valor = document.getElementById('inputVocales').value;
  var resultado = 0;
  var valorArr = valor.split('');
  for (var i = 0; i < valorArr.length; i++) {
    for (var j = 0; j < vocales.length; j++) {
      if (valorArr[i] == vocales[j]) {
        resultado++;
      }
    }
  }
  document.getElementById('resultadoVocales').innerText = resultado;
}

function cambiarMayus() {
  var valor = document.getElementById('inputMayus').value;
  document.getElementById('resultadoMayus').innerText = valor.toUpperCase();
}

function calcularLetraDni() {
  var input = document.getElementById('inputDni');
  var valor = input.value;
  var resultado = document.getElementById('resultadoDni');
  if (valor.length != 8 || isNaN(valor)) {
    input.style.backgroundColor = 'red';
    resultado.innerText = 'Introduzca ocho números';
    resultado.style.color = 'red';
  } else {
    input.style.backgroundColor = '';
    resultado.innerText = letrasDni[valor % 23];
    resultado.style.color = '';
  }
}

function generarRectangulo() {
  var ancho = document.getElementById('rectAncho').value;
  var alto = document.getElementById('rectAlto').value;
  if (!isNaN(ancho) && !isNaN(alto)) {
    var rect = document.getElementById('rectangulo');
    rect.style.width = ancho + 'px';
    rect.style.height = alto + 'px';
    rect.style.backgroundColor = 'red';
  }
}

function cambiarColorRect(color) {
  var rect = document.getElementById('rectangulo');
  rect.style.backgroundColor = color;
}
