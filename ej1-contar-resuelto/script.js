/*
 * Ejercicio de JavaScript: contar palabras, vocales, consonantes
 * y símbolos de un campo de texto.
 */

var vocalsArray = ['a','e','i','o','u'];
var consonantsArray = 'bcdfghjklmnñpqrstvwxyz'.split('');

function countWords() {
    let text = document.getElementsByTagName('textarea')[0].value;
    let wordsArray = text.split(' ');
    let wordsCount = 0;
    for (let word of wordsArray) {
        if (word != '') wordsCount++;
    }
    document.getElementById('countResults').innerText =
        'Palabras: ' + wordsCount;
    return wordsCount;
}

function countVocals() {
    let text = document.getElementsByTagName('textarea')[0].value;
    let charsArray = text.split('');
    let vocalsCount = 0;
    for (let char of charsArray) {
        for (let vocal of vocalsArray) {
            if (char == vocal) vocalsCount++;
        }
    }
    document.getElementById('countResults').innerText =
        'Vocales: ' + vocalsCount;
    return vocalsCount;
}

function countConsonants() {
    let text = document.getElementsByTagName('textarea')[0].value;
    let charsArray = text.split('');
    let consonantsCount = 0;
    for (let char of charsArray) {
        for (let consonant of consonantsArray) {
            if (char == consonant) consonantsCount++;
        }
    }
    document.getElementById('countResults').innerText =
        'Consonantes: ' + consonantsCount;
    return consonantsCount;
}

function countSymbols() {
    let text = document.getElementsByTagName('textarea')[0].value;
    let charsArray = text.split('');
    let symbolsCount = 0;
    for (let char of charsArray) {
        let found = false;
        for (let vocal of vocalsArray) {
            if (char == vocal) found = true;
        }
        for (let consonant of consonantsArray) {
            if (char == consonant) found = true;
        }
        if (!found) symbolsCount++;
    }
    document.getElementById('countResults').innerText =
        'Símbolos: ' + symbolsCount;
    return symbolsCount;
}

function countAll() {
    let wordsCount = this.countWords();
    let vocalsCount = this.countVocals();
    let consonantsCount = this.countConsonants();
    let symbolsCount = this.countSymbols();
    let result =
        'Palabras: ' + wordsCount + '\n' +
        'Vocales: ' + vocalsCount + '\n' +
        'Consonantes: ' + consonantsCount + '\n' +
        'Símbolos: ' + symbolsCount;
    document.getElementById('countResults').innerText = result;
}
