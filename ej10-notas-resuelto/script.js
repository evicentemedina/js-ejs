var maxInputLength = 120;
var notes = [];

var inputNote = document.getElementsByTagName('input')[0];
var spanCharCounter = document.getElementsByTagName('span')[0];
var sectionNotes = document.getElementsByTagName('section')[0];

inputNote.maxLength = maxInputLength;

// Cuando el input está activo
inputNote.onfocus = function() {
  spanCharCounter.style.visibility = 'visible';
}

// Cuando el input pierde el foco
inputNote.onblur = function() {
  spanCharCounter.style.visibility = 'hidden';
}

// Cuando el valor del input cambia
inputNote.oninput = function() {
  spanCharCounter.innerText = inputNote.value.length + '/' + maxInputLength;
}

// Cuando se pulsa cualquier tecla
inputNote.onkeypress = function(event) {
  if (event.key == 'Enter' && inputNote.value != '') {
    notes.push(inputNote.value);
    inputNote.value = '';
    inputNote.oninput();
    sectionNotes.innerHTML = '';
    for (let i = notes.length - 1; i >= 0; i--) {
      sectionNotes.innerHTML += '<div>' + notes[i] + '</div>';
    }
  }
}

// Damos foco por defecto al input
inputNote.focus();
