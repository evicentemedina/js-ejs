var inputs = document.getElementsByTagName('input');
var button = document.getElementsByTagName('button')[0];
var result = document.getElementById('resultado');
var results = [];
var resultsCount = [];

button.onclick = function() {
  let num1 = inputs[0].value;
  let num2 = inputs[1].value;
  if (isNaN(num1) || isNaN(num2)) {
    result.innerText = 'Introduzca valores numéricos.';
  } else if (num1 == num2) {
    result.innerText = 'Introduzca números diferentes.';
  } else if (num1 > num2) {
    result.innerText = 'El primer número debe ser menor que el segundo.';
  } else {
    let decimals1 = 0;
    let decimals2 = 0;
    if (num1.split('.').length > 1)
      decimals1 = num1.split('.')[1].length;
    if (num2.split('.').length > 1)
      decimals2 = num2.split('.')[1].length;
    let decimals = decimals1;
    if (decimals2 > decimals1)
      decimals = decimals2;
    num1 = Number(num1);
    num2 = Number(num2);
    let res = (Math.random() * (num2 - num1) + num1).toFixed(decimals);
    result.innerText = 'Resultado actual: ' + res
      + '\nResultados anteriores: ' + results;
    //results.push(res);
    let found = false;
    for (let i = 0; i < results.length; i++) {
      if (results[i] == res) {
        resultsCount[i]++;
        found = true;
      }
    }
    if (!found) {
      results.push(res);
      resultsCount.push(1);
    }
    let table = '<table><tr><td>Num</td><td>Count</td></tr>';
    for (let i = 0; i < results.length; i++) {
      table += '<tr><td>' + results[i] + '</td><td>' + resultsCount[i] + '</td></tr>';
    }
    table += '</table>';
    result.innerHTML += '<br>' + table;
  }
}
