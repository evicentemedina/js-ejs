var links = document.getElementsByTagName('a');
var sections = document.getElementsByTagName('section');

var selectInput = document.getElementById('selectInput');

for (let i = 0; i < sections.length; i++) {
  sections[i].style.visibility = 'hidden';
}

function cambiarSeccion(n) {
  for (let i = 0; i < links.length; i++) {
    if (i == n) {
      links[i].style.backgroundColor = 'cyan';
      sections[i].style.visibility = 'visible';
    } else {
      links[i].style.backgroundColor = 'lightblue';
      sections[i].style.visibility = 'hidden';
    }
  }
}

function cambiarSelect() {
  alert(selectInput.value);
}

function pedir() {
  var texto = prompt('Nombre:');
  alert(texto);
}

function confirmar() {
  var aceptado = confirm('¿Acepta?');
  alert(aceptado);
}

function sumar(num1, num2) {
  if(isNaN(num1) || isNaN(num2))
    return;
  alert(Number(num1) + Number(num2));
}
