// Cogemos ambos inputs y los guardamos en un array
var inputs = document.getElementsByTagName('input');
// Guardamos el primero, el de fecha, en una variable
var inputDate = inputs[0];
// Guardamos el segundo, el de DNI, en otra variable
var inputDni = inputs[1];
// Cogemos el botón de validar y lo guardamos en una variable
var btnValidate = document.getElementsByTagName('button')[0];
// Array de letras de DNI ordenadas para validación
var dniLetters = 'TRWAGMYFPDXBNJZSQVHLCKET'.split('');

// Función para transformar la letra introducida en el input de DNI a mayúscula
function mayus() {
    inputDni.value = inputDni.value.toUpperCase();
}

// Función de validación de fecha. Se le pasa una string y devuelve falso
// si incumple algo y verdadero si ha pasado todas las condiciones
function validateDate(string) {
    // Se puede tratar una string como un array de caracteres directamente
    // sin necesidad de hacer un .split('')
    // Comprobamos que tenga entre 6 y 10 caracteres
    if (string.length < 6 || string.length > 10)
        return false;
    // La dividimos por los guiones para obtener un array con los tres campos:
    // día, mes y año
    let dateArr = string.split('-');
    // Si no tiene tres campos, está mal
    if (dateArr.length != 3)
        return false;
    // Si el primer campo no tiene uno o dos caracteres, está mal
    if (dateArr[0].length < 1 || dateArr[0].length > 2)
        return false;
    // Lo mismo, con el segundo campo, el de mes
    if (dateArr[1].length < 1 || dateArr[1].length > 2)
        return false;
    // Si el tercer campo, el de año, no tiene 2 o 4 caracteres, está mal
    // Formato corto: 18 Formato completo: 2018
    if (dateArr[2].length != 4 && dateArr[2].length != 2)
        return false;
    // Recorremos los tres campos comprobando si el contenido de alguno de
    // ellos no es un número
    for (let i of dateArr)
        if (isNaN(i))
            return false;
    // Pasamos el mes a tipo numérico y comprobamos que sea entre 1 y 12
    let month = Number(dateArr[1]);
    if (month < 1 || month > 12)
        return false;
    // Pasamos el día a tipo numérico y comprobamos que sea entre 1 y 31
    let day = Number(dateArr[0]);
    if (day < 1 || day > 31)
        return false;
    // Si el mes es Febrero, el día no puede ser mayor de 28
    if (month == 2 && day > 28)
        return false;
    // Si el mes es Abril, Junio, Septiembre o Noviembre, el día no puede ser
    // mayor de 30
    if (
        (month == 4 || month == 6 || month == 9 || month == 11)
        && day > 30
    )
        return false;
    // Pasamos el año a tipo numérico y comprobamos que no sea negativo
    let year = Number(dateArr[2]);
    if (year < 0)
        return false;
    // Si se han cumplido todas las condiciones, está 'correcta'
    return true;
}

// Función de validación de DNI. Se le pasa un string y devuelve falso si es
// incorrecto y verdadero si es válido
function validateDni(string) {
    // Comprobamos que la string tenga nueve caracteres
    if (string.length != 9)
        return false;
    // Recorremos los ocho primeros caracteres de la string y comprobamos
    // que ninguno sea no numérico
    for (let i = 0; i < 8; i++)
        if (isNaN(string[i]))
            return false;
    // Comprobamos que el noveno carácter, la letra, sea la que tendría que
    // corresponder según el número del DNI
    if (string[8] === dniLetters[Number(string.substring(0, 8)) % 23])
        return true;
    // Si lo anterior no se ha cumplido, devolvemos falso
    return false;
}

// Función de click del botón validar
btnValidate.onclick = function() {
    // Llamamos a la función de validación de fecha pasándole el valor actual
    // del input de fecha y guardamos lo que devuelva en una variable
    let isDateValid = validateDate(inputDate.value);
    // Lo mismo, pero con el DNI
    let isDniValid = validateDni(inputDni.value);
    // Dependiendo de si las funciones han devuelto true o false, cambiamos el
    // color de fondo del input correspondiente a verde o rojo
    if (isDateValid)
        inputDate.style.backgroundColor = 'green';
    else
        inputDate.style.backgroundColor = 'red';
    if (isDniValid)
        inputDni.style.backgroundColor = 'green';
    else
        inputDni.style.backgroundColor = 'red';
}
