// Cogemos todos los párrafos y los asignamos a un array:
var paragraphs = document.getElementsByTagName('p');

// Cogemos el botón y lo guardamos en una variable:
var button = document.getElementsByTagName('button')[0];

// Cogemos el span de resultado, cogiendo el único div que hay
// y, de su contenido, cogiendo el único span que hay:
var result = document.getElementsByTagName('div')[0]
    .getElementsByTagName('span')[0];

// Función de contar palabras, para facilitar el proceso ya que
// se ha de realizar varias veces:
function countWords(element) {
    // Dividimos el texto que contenga el elemento por espacios
    // y guardamos el array que devuelve en una variable:
    let wordsArray = element.innerText.split(' ');
    // Iniciamos el contador de palabras a 0:
    let wordsCount = 0;
    // Recorremos el array de palabras:
    for (let word of wordsArray) {
        // Si la palabra no es una cadena de texto vacía:
        if (word !== '') {
            // Sumamos uno al contador de palabras:
            wordsCount++;
        }
    }
    // Devolvemos el valor del contador:
    return wordsCount;
}

// Función del click del botón:
button.onclick = function() {
    // Vaciamos el texto del resultado para borrar el anterior:
    result.innerText = null;
    // Iniciamos un array vacío para que sirva como los tres contadores:
    let negritas = [];
    // Recorremos el array de los tres párrafos:
    for (let i = 0; i < paragraphs.length; i++) {
        // Iniciamos el contador de ese párrafo a 0:
        negritas[i] = 0;
        // Obtenemos todos los elementos con la clase 'negrita' que haya
        // en ese párrafo y los guardamos en un array:
        let negritasByClass = paragraphs[i].getElementsByClassName('negrita');
        // Obtenemos todos los elementos con la etiqueta 'b' que haya
        // en ese párrafo y los guardamos en otro array:
        let negritasByTag = paragraphs[i].getElementsByTagName('b');
        // Recorremos el array de elementos con la clase 'negrita':
        for (let j = 0; j < negritasByClass.length; j++) {
            // Iniciamos una variable a falso para comprobar si existe
            // dos veces el elemento:
            let isDuplicated = false;
            // Recorremos el array de elementos 'b':
            for (let k = 0; k < negritasByTag.length; k++) {
                // Si se trata de la primera vez:
                if (j === 0) {
                    // Suma las palabras que sean negrita
                    // por la etiqueta 'b':
                    negritas[i] += countWords(negritasByTag[k]);
                }
                // Comprueba si el elemento es el mismo en los
                // dos arrays, es decir, si es una etiqueta 'b'
                // con la clase 'negrita':
                if (negritasByClass[j] === negritasByTag[k]) {
                    isDuplicated = true;
                }
            }
            // Una vez sabemos que no está duplicado:
            if (!isDuplicated) {
                // Suma las palabras que sean negrita por la
                // clase 'negrita':
                negritas[i] += countWords(negritasByClass[j]);
            }
        }
        // Añade el resultado de ese párrafo al span del resultado:
        result.innerText += 'Párrafo ' + (i + 1) + ': ' + negritas[i] + '\n';
    }
}
