var titles = document.getElementsByTagName('h1');
var paragraphs = document.getElementsByTagName('p');

function showElementsInConsole() {
    console.log('Títulos:');
    console.log(titles);
    for (let i = 0; i < titles.length; i++) {
        console.log(titles[i].innerText);
    }
    console.log('Párrafos:');
    console.log(paragraphs);
    for (let i = 0; i < paragraphs.length; i++) {
        console.log(paragraphs[i].innerText);
    }
}

//this.showElementsInConsole();

function changeColor() {
    let selectedValue = document
        .getElementById('selectColor').value;
    let body = document.body;
    let color1;
    let color2;
    switch (selectedValue) {
        case '0':
            color1 = 'black';
            color2 = 'white';
            break;
        case '1':
            color1 = 'white';
            color2 = 'black';
            break;
        case '2':
            color1 = 'lightgreen';
            color2 = 'black';
            break;
        default:
            alert('Seleccione una opción');
    }
    body.style = 'color: ' + color1 + '; background-color: '
        + color2 + ';';
}

function countWords() {
    let text = document.getElementsByTagName('textarea')[0].value;
    let wordsArray = text.split(' ');
    let wordsCount = 0;
    for (let word of wordsArray) {
        if (word != '') wordsCount++;
    }
    document.getElementById('countResults').innerText =
        'Palabras: ' + wordsCount;
    return wordsCount;
}

var vocalsArray = ['a','e','i','o','u'];

function countVocals() {
    let text = document.getElementsByTagName('textarea')[0].value;
    let charsArray = text.split('');
    let vocalsCount = 0;
    for (let char of charsArray) {
        for (let vocal of vocalsArray) {
            if (char == vocal) vocalsCount++;
        }
    }
    document.getElementById('countResults').innerText =
        'Vocales: ' + vocalsCount;
    return vocalsCount;
}

var consonantsArray = 'bcdfghjklmnñpqrstvwxyz'.split('');

function countConsonants() {
    let text = document.getElementsByTagName('textarea')[0].value;
    let charsArray = text.split('');
    let consonantsCount = 0;
    for (let char of charsArray) {
        for (let consonant of consonantsArray) {
            if (char == consonant) consonantsCount++;
        }
    }
    document.getElementById('countResults').innerText =
        'Consonantes: ' + consonantsCount;
    return consonantsCount;
}

function countSymbols() {
    let text = document.getElementsByTagName('textarea')[0].value;
    let charsArray = text.split('');
    let symbolsCount = 0;
    for (let char of charsArray) {
        let found = false;
        for (let vocal of vocalsArray) {
            if (char == vocal) found = true;
        }
        for (let consonant of consonantsArray) {
            if (char == consonant) found = true;
        }
        if (!found) symbolsCount++;
    }
    document.getElementById('countResults').innerText =
        'Símbolos: ' + symbolsCount;
    return symbolsCount;
}

function countAll() {
    let wordsCount = this.countWords();
    let vocalsCount = this.countVocals();
    let consonantsCount = this.countConsonants();
    let symbolsCount = this.countSymbols();
    let result =
        'Palabras: ' + wordsCount + '\n' +
        'Vocales: ' + vocalsCount + '\n' +
        'Consonantes: ' + consonantsCount + '\n' +
        'Símbolos: ' + symbolsCount;
    document.getElementById('countResults').innerText = result;
}
